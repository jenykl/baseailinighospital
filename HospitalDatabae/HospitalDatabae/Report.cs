﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalDatabae
{
    public partial class Report : Form
    {
        public int k;

        AilingObject ob = new AilingObject();
        Pacient t = new Pacient();

        public Report()
        {
            InitializeComponent();
            k = MainMenu.k;
        }

        private void Report_Load(object sender, EventArgs e)
        {

            t = ob.ReturnPacient(k);

            label7.Text = dateTimePicker1.Text;
            label8.Text = dateTimePicker2.Text;

            label1.Text = t.Name;
            label2.Text = t.Surname;
            label3.Text = t.Patronymic;

            label4.Text = t.Diagnoz;
            label5.Text = t.NumberRoom;
            label6.Text = t.ColDays;

            label7.Text = t.DateIn;
            label8.Text = t.DateOut;

        }

        private void Report_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

    }
}
