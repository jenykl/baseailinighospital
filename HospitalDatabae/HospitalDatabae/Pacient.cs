﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalDatabae
{
    [Serializable]
    public class Pacient : Person
    {
        private string numberroom;
        private string dateout;
        private string datein;
        private string diagnoz;
        private string coldays;

        public string Diagnoz
        {
            get { return diagnoz; }
            set { diagnoz = value; }
        }

        public string DateIn
        {
            get { return datein; }
            set { datein = value; }
        }

        public string DateOut
        {
            get { return dateout; }
            set { dateout = value; }
        }

        public string NumberRoom
        {
            get { return numberroom; }
            set { numberroom = value; }
        }

        public string ColDays
        {
            get { return coldays; }
            set { coldays = value; }
        }

        public Pacient()
            : base()
        { }

    }
}
