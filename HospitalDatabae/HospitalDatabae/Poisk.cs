﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalDatabae
{

    public partial class Poisk : Form
    {
        private MainMenu tMainMenu;
        public Poisk(MainMenu tMainMenu)
        {
            InitializeComponent();
            this.tMainMenu = tMainMenu;
        }

        private void button7_Click(object sender, EventArgs e)
        {

            try
            {
                bool exists = false;

                int l = tMainMenu.dataGridView1.Rows.Count;
                if (l == 0)
                {
                    MessageBox.Show("А где искать-то?!", "Ошибка", 0, MessageBoxIcon.Error);
                    textBox2.Clear();
                    return;
                }

                if (string.IsNullOrWhiteSpace(textBox2.Text) || textBox2.Text == ToString())
                {
                    MessageBox.Show("Поле ''Поиск'' пустое!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox2.Focus();
                    return;
                }

                else

                    for (int i = 0; i < this.tMainMenu.dataGridView1.RowCount; i++)
                    {
                        tMainMenu.dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        if (tMainMenu.dataGridView1.Rows[i].Cells[comboBox1.Text].Value.ToString().Contains(textBox2.Text))
                        {
                            exists = true;
                            tMainMenu.dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;

                        }
                    }
                if (!exists)
                {
                    MessageBox.Show("Ничего не найдено!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox2.Clear();

                }

            }
            catch (Exception)
            {
                MessageBox.Show("Выберите поле для поиска!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }

        private void Poisk_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button7.PerformClick();
            }
        }

        private void Poisk_Load(object sender, EventArgs e)
        {
            comboBox1.Text = "Фамилия";
        }

    }

}
