﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;

namespace HospitalDatabae
{
    public partial class RegKey : Form
    {
        public string RegKeyPassword = "qwerty123";
        public RegKey()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult DlgResult = MessageBox.Show("Вы уверены, что хотите выйти из окна ввода ключа безопасности?", "Подтвердите действие", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DlgResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else if (DlgResult == DialogResult.No)
            {
                DlgResult = DialogResult.None;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text) || textBox1.Text == ToString())
            {
                MessageBox.Show("Поле ''Ключ безопасности'' пустое!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.Focus();
                return;
            }

            if (textBox1.Text == RegKeyPassword)
            {
                this.Hide();
                MainMenu f3 = new MainMenu();
                f3.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Неверный ключ безопасности!\nВведите заново.", "Ошибка", 0, MessageBoxIcon.Error);
                textBox1.Clear();
                textBox1.Focus();
                return;
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox1.Text == RegKeyPassword)
                {
                    this.Hide();
                    MainMenu f3 = new MainMenu();
                    f3.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Неверный ключ безопасности!\nВведите заново.", "Ошибка", 0, MessageBoxIcon.Error);
                    textBox1.Clear();
                    return;
                }
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = e.KeyChar == (char)Keys.Enter;
            }
        }

    }

}
