﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalDatabae
{
    public partial class Filtr : Form
    {
        private MainMenu tMainMenu;
        public Filtr(MainMenu tMainMenu)
        {
            InitializeComponent();
            this.tMainMenu = tMainMenu;
        }

        private void Filtr_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bool exists = false;

            int l = tMainMenu.dataGridView1.Rows.Count;
            if (l == 0)
            {
                MessageBox.Show("А где фильтровать-то?!", "Ошибка", 0, MessageBoxIcon.Error);
                textBox1.Clear();
                return;
            }

            if (string.IsNullOrWhiteSpace(textBox1.Text) || textBox1.Text == ToString())
            {
                MessageBox.Show("Поле ''Фильтр'' пустое!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.Focus();
                return;
            }


            for (int i = 0; i < tMainMenu.dataGridView1.RowCount; i++)
            {
                for (int j = 0; j < tMainMenu.dataGridView1.ColumnCount; j++)
                {

                    if (tMainMenu.dataGridView1.Rows[i].Cells[j].Value == null)
                    {
                        break;
                    }

                    if (tMainMenu.dataGridView1.Rows[i].Cells[j].Value.ToString().Contains(textBox1.Text))
                    {
                        tMainMenu.dataGridView1.Rows[i].Visible = true;
                        exists = true;
                        break;

                    }
                    else
                    {
                        tMainMenu.dataGridView1.Rows[i].Visible = false;
                    }

                }

            }

            if (!exists)
            {
                MessageBox.Show("Ничего не найдено!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.Clear();

            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button4.PerformClick();
            }
        }
    }
}
