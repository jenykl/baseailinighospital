﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace HospitalDatabae
{
    [Serializable]
    public class AilingObject
    {
        public string str;

        static public List<Pacient> PacientList = new List<Pacient>();

        static public int k = 0;

        public void AddPacient(Pacient t)
        {
            PacientList.Add(t);
            k++;
        }

        public override string ToString()
        {
            return k.ToString();
        }

        public void RemovePacient(int i)
        {
            PacientList.RemoveRange(i, 1);
        }

        public int CountPacient()
        {
            return PacientList.Count;
        }

        public Pacient ReturnPacient(int i)
        {

            return PacientList[i];

        }

        public void PacientSerializable()
        {
            SaveFileDialog filesave = new SaveFileDialog() { Filter = "BIN|*.bin" };

            if (filesave.ShowDialog() == DialogResult.OK && !String.IsNullOrEmpty(filesave.FileName))
            {
                try
                {
                    FileStream fs = new FileStream(filesave.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(fs, PacientList);
                    fs.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "Невозможно сохранить файл!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        public void PacientDeserialize()
        {

            OpenFileDialog fileopen = new OpenFileDialog() { Filter = "BIN|*.bin" };

            if (fileopen.ShowDialog() == DialogResult.OK && !String.IsNullOrEmpty(fileopen.FileName))
                try
                {
                    FileStream fs = new FileStream(fileopen.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    BinaryFormatter bf = new BinaryFormatter();
                    PacientList = (List<Pacient>)bf.Deserialize(fs);
                    fs.Close();
                }

                catch (Exception)
                {
                    MessageBox.Show("Произошел сбой при открытии файла!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

        }

    }

}
