﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace HospitalDatabae
{
    public partial class MainMenu : Form
    {
        public static int k;

        AilingObject ob = new AilingObject();
        Pacient t = new Pacient();

        public MainMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Ailing b1 = new Ailing();
            b1.ShowDialog();

            AilingObject ob = new AilingObject();
            Pacient t = new Pacient();

            dataGridView1.Rows.Clear();

            for (int i = 0; i < ob.CountPacient(); i++)
            {
                t = ob.ReturnPacient(i);
                DataGridViewCell firstCell = new DataGridViewTextBoxCell();
                DataGridViewCell secondCell = new DataGridViewTextBoxCell();
                DataGridViewCell thirdCell = new DataGridViewTextBoxCell();
                DataGridViewCell fourthCell = new DataGridViewTextBoxCell();
                DataGridViewCell fifthCell = new DataGridViewTextBoxCell();
                DataGridViewCell sixthCell = new DataGridViewTextBoxCell();
                DataGridViewCell seventhCell = new DataGridViewTextBoxCell();

                DataGridViewRow row = new DataGridViewRow();

                firstCell.Value = t.Name;
                secondCell.Value = t.Surname;
                thirdCell.Value = t.Patronymic;
                fourthCell.Value = t.Diagnoz;
                fifthCell.Value = t.DateIn;
                sixthCell.Value = t.DateOut;
                seventhCell.Value = t.NumberRoom;

                row.Cells.AddRange(firstCell, secondCell, thirdCell, fourthCell, fifthCell, sixthCell, seventhCell);
                this.dataGridView1.Rows.Add(row);

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ClearSelection();

            int l = dataGridView1.Rows.Count;
            if (l == 0)
            {
                MessageBox.Show("Попытка удалить несуществующую запись!", "Ошибка", 0, MessageBoxIcon.Error);
            }

            else
            {

                var result = MessageBox.Show("Удалить выделенную информацию?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    k = dataGridView1.CurrentRow.Index;
                    t = ob.ReturnPacient(k);
                    ob.RemovePacient(k);
                    dataGridView1.Rows.Clear();
                    for (int i = 0; i < ob.CountPacient(); i++)
                    {
                        t = ob.ReturnPacient(i);
                        DataGridViewCell firstCell = new DataGridViewTextBoxCell();
                        DataGridViewCell secondCell = new DataGridViewTextBoxCell();
                        DataGridViewCell thirdCell = new DataGridViewTextBoxCell();
                        DataGridViewCell fourthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell fifthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell sixthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell seventhCell = new DataGridViewTextBoxCell();

                        DataGridViewRow row = new DataGridViewRow();

                        firstCell.Value = t.Name;
                        secondCell.Value = t.Surname;
                        thirdCell.Value = t.Patronymic;
                        fourthCell.Value = t.Diagnoz;
                        fifthCell.Value = t.DateIn;
                        sixthCell.Value = t.DateOut;
                        seventhCell.Value = t.NumberRoom;

                        row.Cells.AddRange(firstCell, secondCell, thirdCell, fourthCell, fifthCell, sixthCell, seventhCell);
                        this.dataGridView1.Rows.Add(row);
                    }

                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            int n = dataGridView1.Rows.Count;
            if (n == 0)
            {
                MessageBox.Show("Попытка редактировать несуществующую запись!", "Ошибка", 0, MessageBoxIcon.Error);
            }

            else
            {
                k = dataGridView1.CurrentCell.RowIndex;

                if (k >= 0)
                {

                    EditAiling win2 = new EditAiling();
                    win2.ShowDialog();
                    this.Show();

                    AilingObject ob = new AilingObject();
                    Pacient t = new Pacient();
                    dataGridView1.Rows.Clear();

                    for (int i = 0; i < ob.CountPacient(); i++)
                    {
                        t = ob.ReturnPacient(i);

                        DataGridViewCell firstCell = new DataGridViewTextBoxCell();
                        DataGridViewCell secondCell = new DataGridViewTextBoxCell();
                        DataGridViewCell thirdCell = new DataGridViewTextBoxCell();
                        DataGridViewCell fourthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell fifthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell sixthCell = new DataGridViewTextBoxCell();
                        DataGridViewCell seventhCell = new DataGridViewTextBoxCell();

                        DataGridViewRow row = new DataGridViewRow();

                        firstCell.Value = t.Name;
                        secondCell.Value = t.Surname;
                        thirdCell.Value = t.Patronymic;
                        fourthCell.Value = t.Diagnoz;
                        fifthCell.Value = t.DateIn;
                        sixthCell.Value = t.DateOut;
                        seventhCell.Value = t.NumberRoom;

                        row.Cells.AddRange(firstCell, secondCell, thirdCell, fourthCell, fifthCell, sixthCell, seventhCell);
                        this.dataGridView1.Rows.Add(row);

                    }

                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Filtr f45 = new Filtr(this);
            f45.Show();
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы действительно хотите выйти из программы?", "Подтвердите действие", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.ExitThread();
            }
            else
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            int m = dataGridView1.Rows.Count;
            try
            {
                if (m == 0)
                {
                    MessageBox.Show("Попытка сделать отчёт без записи!", "Ошибка", 0, MessageBoxIcon.Error);
                }

                else
                {
                    k = dataGridView1.CurrentCell.RowIndex;

                    if (k >= 0)
                    {

                        Report win2 = new Report();
                        win2.ShowDialog();
                        this.Show();

                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Выделите запись для составления отчёта!", "Обратите внимание", 0, MessageBoxIcon.Asterisk);
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ob.PacientSerializable();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {

            dataGridView1.Rows.Clear();

            ob.PacientDeserialize();

            for (int i = 0; i < ob.CountPacient(); i++)
            {
                t = ob.ReturnPacient(i);
                DataGridViewCell firstCell = new DataGridViewTextBoxCell();
                DataGridViewCell secondCell = new DataGridViewTextBoxCell();
                DataGridViewCell thirdCell = new DataGridViewTextBoxCell();
                DataGridViewCell fourthCell = new DataGridViewTextBoxCell();
                DataGridViewCell fifthCell = new DataGridViewTextBoxCell();
                DataGridViewCell sixthCell = new DataGridViewTextBoxCell();
                DataGridViewCell seventhCell = new DataGridViewTextBoxCell();

                DataGridViewRow row = new DataGridViewRow();

                firstCell.Value = t.Name;
                secondCell.Value = t.Surname;
                thirdCell.Value = t.Patronymic;
                fourthCell.Value = t.Diagnoz;
                fifthCell.Value = t.DateIn;
                sixthCell.Value = t.DateOut;
                seventhCell.Value = t.NumberRoom;

                row.Cells.AddRange(firstCell, secondCell, thirdCell, fourthCell, fifthCell, sixthCell, seventhCell);
                this.dataGridView1.Rows.Add(row);

            }

        }

        private void button6_Click(object sender, EventArgs e)
        {

            int n = dataGridView1.Rows.Count;
            if (n == 0)
            {
                MessageBox.Show("Попытка редактировать несуществующую запись!", "Ошибка", 0, MessageBoxIcon.Error);
            }

            else
            {
                dataGridView1.Rows.Clear();

                for (int i = 0; i < ob.CountPacient(); i++)
                {
                    t = ob.ReturnPacient(i);
                    DataGridViewCell firstCell = new DataGridViewTextBoxCell();
                    DataGridViewCell secondCell = new DataGridViewTextBoxCell();
                    DataGridViewCell thirdCell = new DataGridViewTextBoxCell();
                    DataGridViewCell fourthCell = new DataGridViewTextBoxCell();
                    DataGridViewCell fifthCell = new DataGridViewTextBoxCell();
                    DataGridViewCell sixthCell = new DataGridViewTextBoxCell();
                    DataGridViewCell seventhCell = new DataGridViewTextBoxCell();

                    DataGridViewRow row = new DataGridViewRow();

                    firstCell.Value = t.Name;
                    secondCell.Value = t.Surname;
                    thirdCell.Value = t.Patronymic;
                    fourthCell.Value = t.Diagnoz;
                    fifthCell.Value = t.DateIn;
                    sixthCell.Value = t.DateOut;
                    seventhCell.Value = t.NumberRoom;

                    row.Cells.AddRange(firstCell, secondCell, thirdCell, fourthCell, fifthCell, sixthCell, seventhCell);
                    this.dataGridView1.Rows.Add(row);

                }
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button4.PerformClick();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Poisk f44 = new Poisk(this);
            f44.Show();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {

        }

    }

}
