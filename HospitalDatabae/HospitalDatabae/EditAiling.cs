﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalDatabae
{

    public partial class EditAiling : Form
    {
        public int k;

        AilingObject ob = new AilingObject();
        Pacient t = new Pacient();

        public EditAiling()
        {
            InitializeComponent();
            k = MainMenu.k;
        }

        private void EditAiling_Load(object sender, EventArgs e)
        {
            t = ob.ReturnPacient(k);
            textBox1.Text = t.Name;
            textBox2.Text = t.Surname;
            textBox3.Text = t.Patronymic;
            dateTimePicker1.Text = t.DateIn;
            dateTimePicker2.Text = t.DateOut;
            textBox7.Text = t.NumberRoom;
            textBox4.Text = t.Diagnoz;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TimeSpan diffdate = dateTimePicker2.Value.Date - dateTimePicker1.Value.Date;
            int datePacient = diffdate.Days;

            if (datePacient == 0)
            {
                datePacient = 1;
            }

            t.ColDays = datePacient.ToString();

            t.Name = textBox1.Text;
            t.Surname = textBox2.Text;
            t.Patronymic = textBox3.Text;
            t.Diagnoz = textBox4.Text;
            t.DateIn = dateTimePicker1.Text;
            t.DateOut = dateTimePicker2.Text;
            t.NumberRoom = textBox7.Text;

            if (string.IsNullOrWhiteSpace(textBox1.Text) || textBox1.Text == ToString())
            {
                MessageBox.Show("Поле ''Фамилия'' не может быть пустым!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(textBox2.Text) || textBox2.Text == ToString())
            {
                MessageBox.Show("Поле ''Имя'' не может быть пустым!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox2.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(textBox3.Text) || textBox3.Text == ToString())
            {
                MessageBox.Show("Поле ''Отчество'' не может быть пустым!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox3.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(textBox4.Text) || textBox4.Text == ToString())
            {
                MessageBox.Show("Поле ''Диагноз'' не может быть пустым!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox4.Focus();
                return;
            }


            if (string.IsNullOrWhiteSpace(textBox7.Text) || textBox7.Text == ToString())
            {
                MessageBox.Show("Поле ''Номер палаты'' не может быть пустым!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox7.Focus();
                return;
            }


            if (dateTimePicker2.Value.Date < dateTimePicker1.Value.Date)
            {
                MessageBox.Show("Дата выписки не может быть раньше даты поступления!", "Обратите внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                dateTimePicker2.Value = dateTimePicker1.Value;
                t.DateOut = dateTimePicker1.Text;
                return;
            }
            else
            {
                ob.RemovePacient(k);
            }

            ob.AddPacient(t);
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetter(e.KeyChar)) return;
            e.Handled = true;
            return;
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetter(e.KeyChar)) return;
            e.Handled = true;
            return;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            var tb = (TextBox)sender;
            if (e.KeyChar.Equals('-'))
            {
                e.Handled = tb.SelectionStart == 0 || tb.Text[tb.SelectionStart - 1].Equals('-');
                if (!e.Handled)
                {
                    return;
                }
            }
            e.Handled = !char.IsLetter(e.KeyChar);
        }

        private void EditAiling_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
                    e.Handled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text.Length == 1)
            {
                ((TextBox)sender).Text = ((TextBox)sender).Text.ToUpper();
                ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
            }

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsDigit(e.KeyChar) || char.IsLetter(e.KeyChar)) return;
            var tb = (TextBox)sender;
            if (e.KeyChar.Equals('-') || e.KeyChar.Equals('.') || e.KeyChar.Equals(',') || e.KeyChar.Equals(' '))
            {
                e.Handled = tb.SelectionStart == 0 || tb.Text[tb.SelectionStart - 1].Equals('-') || tb.Text[tb.SelectionStart - 1].Equals('.') || tb.Text[tb.SelectionStart - 1].Equals(',') || tb.Text[tb.SelectionStart - 1].Equals(' ');
                if (!e.Handled)
                {
                    return;
                }
            }
            e.Handled = !char.IsLetter(e.KeyChar);

            if (e.KeyChar == '-' || e.KeyChar == ',' || e.KeyChar == '.' || e.KeyChar == ' ')
                return;
            else
                e.Handled = true;
        }

    }

}
